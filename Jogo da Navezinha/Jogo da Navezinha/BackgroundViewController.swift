//
//  BackgroundViewController.swift
//  Jogo da Navezinha
//
//  Created by Allison Lindner on 06/04/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

import UIKit
import SpriteKit

class BackgroundViewController: UIViewController {
	
	var bgScene: BackgroundScene!
	
	override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		
		let bgSKView = self.view as SKView;
		
		bgScene = BackgroundScene(size: bgSKView.bounds.size);
		bgScene.scaleMode = .AspectFill;
		bgScene.physicsWorld.gravity = CGVector(dx: 0, dy: 0);
		
		bgSKView.presentScene(bgScene);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
