//
//  GameViewController.swift
//  Jogo da Navezinha
//
//  Created by Allison Lindner on 01/04/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

import UIKit
import SpriteKit

class GameViewController: UIViewController {

	var appDelegate: AppDelegate!
	var scene: GameScene!
	
    override func viewDidLoad() {
		super.viewDidLoad();
	
		appDelegate = UIApplication.sharedApplication().delegate as AppDelegate;
		
		let skView = self.view as SKView;
		skView.multipleTouchEnabled = true;
		skView.showsFPS = false;
		skView.showsPhysics = false;
		
		//skView.frameInterval = 60/30;
		
		scene = GameScene(size: skView.bounds.size);
		scene.scaleMode = .AspectFill;
		scene.physicsWorld.gravity = CGVector(dx: 0, dy: 0);
		
		scene.physicsBody = SKPhysicsBody(edgeLoopFromRect: skView.frame);
		scene.physicsBody!.restitution = 0.0;
		
		scene.physicsBody!.categoryBitMask		= BodyType.wall.rawValue;
		scene.physicsBody!.collisionBitMask		= BodyType.player.rawValue;
		scene.physicsBody!.contactTestBitMask	= BodyType.player.rawValue;
		
		var endOfScene = SKSpriteNode();
		endOfScene.size		= CGSize(width: 2, height: skView.frame.height);
		endOfScene.color	= UIColor.blackColor();
		endOfScene.position = CGPoint(x: skView.frame.width, y: skView.frame.height/2);
		
		endOfScene.physicsBody = SKPhysicsBody(rectangleOfSize: endOfScene.size);
		endOfScene.physicsBody!.affectedByGravity = false;
		
		endOfScene.physicsBody!.categoryBitMask		= BodyType.endOfScene.rawValue;
		endOfScene.physicsBody!.collisionBitMask	= 0;
		endOfScene.physicsBody!.contactTestBitMask	= 0;
		
		scene.addChild(endOfScene);
		appDelegate.manager.gameViewController = self;
		
		skView.presentScene(scene);
		
		var swipeGestureRecognizer: UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: "swipeGesture:");
		
		//gestureRecognizer.direction = UISwipeGestureRecognizerDirection.Up;
		skView.addGestureRecognizer(swipeGestureRecognizer);
    }

    override func shouldAutorotate() -> Bool {
        return true
    }

    override func supportedInterfaceOrientations() -> Int {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return Int(UIInterfaceOrientationMask.AllButUpsideDown.rawValue)
        } else {
            return Int(UIInterfaceOrientationMask.All.rawValue)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
	
	func swipeGesture(recognizer:UIPanGestureRecognizer) {
		
		if(recognizer.state == UIGestureRecognizerState.Began) {
			self.scene.timeForShoot = 0.0;
			self.scene.playerShooting = true;
		}
		
		if(recognizer.state == UIGestureRecognizerState.Changed) {
			self.scene.playerShooting = true;
			self.scene.player.sprite.physicsBody?
				.applyImpulse(CGVector(	dx:							0						  ,
										dy: -(recognizer.velocityInView(self.view).y * 0.0015 )));
		}
		
		if(recognizer.state == UIGestureRecognizerState.Ended) {
			self.scene.playerShooting = false;
		}
	}
}
