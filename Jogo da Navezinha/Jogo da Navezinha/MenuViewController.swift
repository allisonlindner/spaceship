//
//  MenuViewController.swift
//  Jogo da Navezinha
//
//  Created by Allison Lindner on 04/04/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

import UIKit
import AVFoundation

class MenuViewController: UIViewController {
	
	@IBOutlet weak var centerYGameOverLabel: NSLayoutConstraint!
	@IBOutlet weak var horizontalLabelButton: NSLayoutConstraint!
	@IBOutlet weak var horizontalButton: NSLayoutConstraint!
	@IBOutlet weak var topSpaceTitle: NSLayoutConstraint!
	@IBOutlet weak var buttonConnect: UIButton!
	@IBOutlet weak var descriptionConnect: UITextView!
	@IBOutlet weak var buttonJogar: UIButton!
	@IBOutlet weak var descriptionJogar: UITextView!
	@IBOutlet weak var labelDescription: UITextView!
	
	var appDelegate: AppDelegate!
	var audioPlayer: AVAudioPlayer?
	
	var gameOver = false;
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		appDelegate = UIApplication.sharedApplication().delegate as AppDelegate;
		appDelegate.manager.menuViewController = self;
		
		var path = NSBundle.mainBundle().pathForResource("background_sound_loop", ofType:"mp3");
		var url = NSURL.fileURLWithPath(path!);
		
		var error: NSError?
		
		audioPlayer = AVAudioPlayer(contentsOfURL: url, error: &error);
		audioPlayer?.numberOfLoops = -1;
		audioPlayer?.play();
    }
	
	override func viewWillAppear(animated: Bool) {
		animations();
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	@IBAction func connectAction(sender: AnyObject) {
		println("Testeeeeeeee");
		appDelegate.manager.networkManager.presentBrowser(self);
	}
	
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		if(segue.identifier == "play") {
			appDelegate.manager.networkManager.distributeNetworkPeerArray();
			
			if(appDelegate.manager.networkManager.isHost) {
				appDelegate.manager.networkManager.sendToPeers(
					appDelegate.manager.networkManager.handler.session.connectedPeers,
					data: NSDictionary(dictionary: ["began" : 1]));
			} else {
				appDelegate.manager.networkManager.isHost = true;
			}
		}
	}
	
	func animations() {
		
		self.buttonConnect.hidden = gameOver;
		self.buttonJogar.hidden = gameOver;
		self.descriptionConnect.hidden = gameOver;
		self.descriptionJogar.hidden = gameOver;
		
		self.horizontalButton.constant = -200;
		self.horizontalLabelButton.constant = 800;
		self.topSpaceTitle.constant = -110;
		self.centerYGameOverLabel.constant = -400;
		self.view.layoutIfNeeded();
		
		if(!gameOver) {
			
			/**********************************
			*** ANIMAÇÕES DO MENU PRINCIPAL ***
			**********************************/
			
			UIView.animateWithDuration(0.5, delay: 1.0 , options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
				
				self.horizontalLabelButton.constant = 0.0;
				self.horizontalButton.constant = 10.0;
				self.view.layoutIfNeeded();
				
			}, completion: nil);
			
			UIView.animateWithDuration(0.5, delay: 1.0 , options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
				
				self.topSpaceTitle.constant = 40.0;
				self.view.layoutIfNeeded();
				
				}, completion: { (finish) -> Void in
					
					UIView.animateWithDuration(0.2, animations: { () -> Void in
						
						self.topSpaceTitle.constant = 20.0;
						self.view.layoutIfNeeded();
					})
			});
		} else {
			
			/*************************************
			******* ANIMAÇÕES DO GAME OVER *******
			*************************************/
			
			UIView.animateWithDuration(0.5, delay: 0.5 , options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
				
				self.centerYGameOverLabel.constant = 30;
				self.view.layoutIfNeeded();
				
				}, completion: { (finish) -> Void in
					
					UIView.animateWithDuration(0.2, animations: { () -> Void in
						
						self.centerYGameOverLabel.constant = 0;
						self.view.layoutIfNeeded();
					})
			});
			
			NSTimer.scheduledTimerWithTimeInterval(3.0, target: self, selector: Selector("restart"), userInfo: nil, repeats: false);
		}
	}
	
	func restart() {
		gameOver = false;
		
		/**************************************
		*** ANIMAÇÕES DE SAIDA DO GAME OVER ***
		**************************************/
		
		UIView.animateWithDuration(0.5, delay: 0.0 , options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
			
			self.topSpaceTitle.constant = -110;
			self.centerYGameOverLabel.constant = -400;
			self.view.layoutIfNeeded();
			
		}, completion: nil);
		
		animations();
	}
}
