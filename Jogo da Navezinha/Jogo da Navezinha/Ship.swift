//
//  Ship.swift
//  Jogo da Navezinha
//
//  Created by Allison Lindner on 02/04/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

import UIKit
import SpriteKit

class Ship: NSObject {
	
	var sprite:		SKShapeNode!
	var propulsion:	Float = 1.0;
	var engine:		Float = 0.0;
	
	var machineGun:	Weapon = Weapon(type: 0);
	
	var type: NSInteger = 0;
	
	var PosX: Float = 0.0;
	var PosY: Float = 0.0;
	
	override init() {
		super.init();
		
		sprite = SKShapeNode();
	}
}
