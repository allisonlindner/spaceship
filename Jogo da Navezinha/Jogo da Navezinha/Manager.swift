//
//  Manager.swift
//  Jogo da Navezinha
//
//  Created by Allison Lindner on 04/04/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

import UIKit
import SpriteKit
import MultipeerConnectivity

class Manager: NSObject, NetworkProtocol {
	
	
	var networkManager: NetworkManager!
	var gameViewController: GameViewController!
	var menuViewController: MenuViewController!
	
	override init() {
		super.init();
		
		networkManager = NetworkManager();
		networkManager.delegate = self;
	}
	
	func onConnect(peer: MCPeerID) {
		
	}
	
	func onConnectError(peer: MCPeerID) {
		
	}
	
	func onReceiveData(data: NSDictionary, peerid: MCPeerID, name: String) {
		
		if(data.objectForKey("began")?.integerValue == 1) {
			
			menuViewController.presentViewController(menuViewController.storyboard?.instantiateViewControllerWithIdentifier("game") as GameViewController, animated: true, completion: nil);
			
		} else if(data.objectForKey("shipType")?.integerValue == 2) {
			
			var enemyShip = EnemyShip();
			enemyShip.PosY = data.objectForKey("PosY")!.floatValue;

			var timeSended = data.objectForKey("time") as CFTimeInterval;
			var delay = NSDate().timeIntervalSince1970 - timeSended;
			
			enemyShip.PosX += enemyShip.engine*Float(delay);
			
			Drawer.DrawShip(enemyShip);
			gameViewController.scene.addChild(enemyShip.sprite);
			gameViewController.scene.arrayOfEnemys.addObject(enemyShip.sprite);
			
		} else if(data.objectForKey("shipType")?.integerValue == 3) {
			var enemyShip = EnemySpyShip();
			enemyShip.PosY = data.objectForKey("PosY")!.floatValue;
			
			var timeSended = data.objectForKey("time") as CFTimeInterval;
			var delay = NSDate().timeIntervalSince1970 - timeSended;
			
			enemyShip.PosX += enemyShip.engine*Float(delay);
			
			Drawer.DrawShip(enemyShip);
			gameViewController.scene.addChild(enemyShip.sprite);
			gameViewController.scene.arrayOfSpyEnemys.addObject(enemyShip.sprite);
			
		} else if(data.objectForKey("shipType")?.integerValue == 4) {
			var enemyShip = EnemyShip();
			enemyShip.isTrap = true;
			enemyShip.type = 2;
			enemyShip.PosY = data.objectForKey("PosY")!.floatValue;
			
			var timeSended = data.objectForKey("time") as CFTimeInterval;
			var delay = NSDate().timeIntervalSince1970 - timeSended;
			
			enemyShip.PosX += enemyShip.engine*Float(delay);
			
			Drawer.DrawShip(enemyShip);
			gameViewController.scene.addChild(enemyShip.sprite);
			gameViewController.scene.arrayOfEnemys.addObject(enemyShip.sprite);
			
		} else if(data.objectForKey("end")?.integerValue == 1) {
			menuViewController.gameOver = true;
			gameViewController.dismissViewControllerAnimated(true, completion: nil);
		
		}
	}
}
