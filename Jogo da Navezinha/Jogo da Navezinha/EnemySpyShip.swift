//
//  EnemySpyShip.swift
//  Jogo da Navezinha
//
//  Created by Allison Lindner on 04/04/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

import UIKit

class EnemySpyShip: Ship {
	
	override init() {
		super.init();
		
		type = 3;
		engine = 220.0;
	}
}
