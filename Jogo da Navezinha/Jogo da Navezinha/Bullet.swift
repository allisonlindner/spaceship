//
//  Bullet.swift
//  Jogo da Navezinha
//
//  Created by Allison Lindner on 02/04/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

import UIKit
import SpriteKit

class Bullet: NSObject {
	
	var sprite: SKSpriteNode!
	
	var damage: NSInteger = 0;
	var speed: Float = 1.0;
}
