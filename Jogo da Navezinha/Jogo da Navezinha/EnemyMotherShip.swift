//
//  EnemyMotherShip.swift
//  Jogo da Navezinha
//
//  Created by Allison Lindner on 02/04/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

import UIKit

class EnemyMotherShip: Ship {
	
	override init() {
		super.init();
		
		type = 1;
		
		PosX = 30;
		PosY = 30;
		
		engine = 0.0;
		propulsion = 0.0;
	}
}
