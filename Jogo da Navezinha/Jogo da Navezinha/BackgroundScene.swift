//
//  BackgroundScene.swift
//  Jogo da Navezinha
//
//  Created by Allison Lindner on 06/04/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

import UIKit
import SpriteKit

class BackgroundScene: SKScene {
	
	var stars: SKEmitterNode!
	
	var enemyMotherShip: EnemyMotherShip = EnemyMotherShip();
	
	var spawner1 = CGPoint(x: -80.0, y: 200);
	var spawner2 = CGPoint(x: -60.0, y: 250);
	var spawner3 = CGPoint(x: -40.0, y: 300);
	var spawner4 = CGPoint(x: -60.0, y: 350);
	
	var enemyShipsOnScreen = 0;
	var arrayOfEnemys = NSMutableArray(capacity: 0);
	
	override func didMoveToView(view: SKView) {
		
		stars = SKEmitterNode(fileNamed: "stars");
		stars.position = CGPoint(x: 0, y: view.bounds.size.height);
		stars.hidden = false;
		
		Drawer.DrawShip(enemyMotherShip);
		
		addChild(stars);
		addChild(enemyMotherShip.sprite);
	}
	
	override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
		
	}
	
	override func update(currentTime: NSTimeInterval) {
		
		if(arrayOfEnemys.count < 4) {
			spawnEnemys();
		}
		
		for(var i = 0; i < arrayOfEnemys.count ; i++) {
			var enemy: EnemyShip = EnemyShip();
			enemy.sprite = arrayOfEnemys.objectAtIndex(i) as SKShapeNode;
			enemy.sprite.physicsBody!.velocity =
				CGVector(dx: CGFloat(enemy.engine), dy: 0);
			
			if(enemy.sprite.position.x - 20 > self.view?.frame.width) {
				arrayOfEnemys.removeObject(enemy.sprite);
			}
		}
		
	}
	
	func spawnEnemys() {
		
		enemyShipsOnScreen = 0;
		var currentRow = 0;
		
		while(enemyShipsOnScreen < 4) {
			var enemy = EnemyShip();
			
			switch(enemyShipsOnScreen%4) {
				
			case 0:
				enemy.PosX = Float(Float(spawner1.x) - (Float(currentRow) * 50));
				enemy.PosY = Float(spawner1.y);
				break;
				
			case 1:
				enemy.PosX = Float(Float(spawner2.x) - (Float(currentRow) * 50));
				enemy.PosY = Float(spawner2.y);
				break;
				
			case 2:
				enemy.PosX = Float(Float(spawner3.x) - (Float(currentRow) * 50));
				enemy.PosY = Float(spawner3.y);
				break;
				
			case 3:
				enemy.PosX = Float(Float(spawner4.x) - (Float(currentRow) * 50));
				enemy.PosY = Float(spawner4.y);
				break;
				
			default:
				break;
			}
			
			Drawer.DrawShip(enemy);
			addChild(enemy.sprite);
			enemy.sprite.physicsBody!.velocity = CGVector(dx: CGFloat(enemy.engine), dy: 0);
			arrayOfEnemys.addObject(enemy.sprite);
			enemyShipsOnScreen++;
			
			if(enemyShipsOnScreen%4 == 0) {
				currentRow++;
			}
		}
	}
}
